package com.naran.retrofit;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.naran.retrofit.Constants.UrlConstants;
import com.naran.retrofit.Model.Notification;
import com.naran.retrofit.Service.APIService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textDetails;
    Button getDataButton;
    Button insertNewButton;
    EditText editName;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textDetails = (TextView) findViewById(R.id.textDetails);
        getDataButton = (Button) findViewById(R.id.getDataButton);
        insertNewButton = (Button) findViewById(R.id.insertButton);
        editName = (EditText) findViewById(R.id.editName);

        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("Please Wait");
        getDataButton.setOnClickListener(this);
        insertNewButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.getDataButton:
                progressDialog.show();
                getNotificationData();

                break;
            case R.id.insertButton:
                progressDialog.show();
                insertNotification();
                break;
        }
    }


    private void getNotificationData() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlConstants.GET_BLOOD_REQUEST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);
        Call<List<Notification>> call = service.getNotificationData();

        call.enqueue(new Callback<List<Notification>>() {
            @Override
            public void onResponse(Call<List<Notification>> call, Response<List<Notification>> response) {

                List<Notification> notification = response.body();
                String details = "";

                for (int i = 0; i < notification.size(); i++) {

                    String full_name = notification.get(i).getFull_name();
                    String blood_type = notification.get(i).getBlood_type();
                    String blood_amount = notification.get(i).getBlood_amount();
                    String contact_number = notification.get(i).getContact_number();
                    String donation_date = notification.get(i).getDonation_date();
                    String donation_place = notification.get(i).getDonation_place();
                    String donation_type = notification.get(i).getDonation_type();
                    String description = notification.get(i).getDescription();

                    details += " \n \n Name : " + full_name + " \n Blood Type : " + blood_type;

                }

                textDetails.setText(details);
                progressDialog.hide();

            }

            @Override
            public void onFailure(Call<List<Notification>> call, Throwable t) {
                progressDialog.hide();

            }
        });

    }

    private void insertNotification() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(UrlConstants.POST_BLOOD_REQUEST_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Notification notification = new Notification();
        notification.setFull_name(editName.getText().toString());

        Call<Notification> call = service.insertNotification(notification.getFull_name());

        call.enqueue(new Callback<Notification>() {
            @Override
            public void onResponse(Call<Notification> call, Response<Notification> response) {
                progressDialog.hide();
            }

            @Override
            public void onFailure(Call<Notification> call, Throwable t) {
                progressDialog.hide();

            }
        });

    }
}


//    private TextView full_name;
//    private TextView blood_type;
//    private TextView contact_number;
//    private TextView donation_date;
//    private TextView donation_place;
//
//  full_name = (TextView) findViewById(R.id.adapter_full_name);
//          blood_type = (TextView) findViewById(R.id.adapter_blood_type);
//          contact_number = (TextView) findViewById(R.id.adapter_donation_contact_number);
//          donation_date = (TextView) findViewById(R.id.adapter_donation_date);
//          donation_place = (TextView) findViewById(R.id.adapter_donation_place);