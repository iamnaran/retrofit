package com.naran.retrofit.Service;

import com.naran.retrofit.Constants.UrlConstants;
import com.naran.retrofit.Model.Notification;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by NaRan on 5/23/17.
 */

public interface APIService {

    @GET(UrlConstants.GET_BLOOD_REQUEST_URL)
    Call<List<Notification>> getNotificationData();

    @FormUrlEncoded
    @POST(UrlConstants.POST_BLOOD_REQUEST_URL)
    Call<Notification> insertNotification(@Field("full_name")String full_name);
}


//
//public class UrlConstants {
//    public static final String POST_BLOOOD_REQUEST_URL = "http://192.168.1.4/final_project/postBloodRequest.php";
//    public static final String GET_BLOOD_REQUEST_URL = "http://192.168.1.4/final_project/getBloodRequest.php";
//    public static final String JSON_ARRAY = "result";
//    public static final String KEY_FULLNAME = "full_name";
//    public static final String KEY_BLOOD_TYPE = "blood_type";
//    public static final String KEY_BLOOD_AMOUNT = "blood_amount";
//    public static final String KEY_CONTACT_NUMBER = "contact_number";
//    public static final String KEY_DONATION_DATE = "donation_date";
//    public static final String KEY_DONATION_PLACE = "donation_place";
//    public static final String KEY_DONATION_TYPE = "donation_type";
//    public static final String KEY_DESCRIPTION = "description";
//
//}