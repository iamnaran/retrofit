package com.naran.retrofit.Constants;

/**
 * Created by NaRan on 5/23/17.
 */

public class UrlConstants {
//    public static final String BASE_URL = "";
    public static final String GET_BLOOD_REQUEST_URL = "http://192.168.100.51/final_project/getBloodRequest.php/";
    public static final String POST_BLOOD_REQUEST_URL = "http://192.168.100.51/final_project/postBloodRequest.php/";

    public static final String KEY_FULLNAME = "full_name";
    public static final String KEY_BLOOD_TYPE = "blood_type";
    public static final String KEY_BLOOD_AMOUNT = "blood_amount";
    public static final String KEY_CONTACT_NUMBER = "contact_number";
    public static final String KEY_DONATION_DATE = "donation_date";
    public static final String KEY_DONATION_PLACE = "donation_place";
    public static final String KEY_DONATION_TYPE = "donation_type";
    public static final String KEY_DESCRIPTION = "description";

}
